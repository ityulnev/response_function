# -*- coding: utf-8 -*-
"""
Created on Wed Jun  3 18:57:31 2020

@author: chris
"""

# %% imports and setup
#import os
#import time # to measure the computational time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# plotting
font = {'size': 10}
plt.rc('font', **font)

# %% import data
transmittance = pd.read_csv(r'transmittance.csv',
                            sep=',',
                            names=['wavel', 'T'],
                            header=None)

density = pd.read_csv(r'density.csv',
                      sep=',',
                      names=['wavel', 'density'],
                      header=None)
density.sort_values(by=['wavel'], inplace=True)

# %%  plot
fig, (ax1) = plt.subplots(1, figsize=(16, 12))
ax2 = ax1.twinx()

titel = 'filter from OPTILAB KFT Budapest'
plt.suptitle(titel)

color = 'C0'
ax1.plot(transmittance['wavel'], transmittance['T'], 'x-', color=color)
ax1.set_xlabel(r'$\lambda (nm)$')
ax1.set_ylabel('transmittance', color=color)
ax1.tick_params(axis='y', labelcolor=color)
ax1.grid(color=color, alpha=0.3)

color = 'C1'
ax2.plot(density['wavel'], density['density'], 'x-', color=color)
ax2.set_ylabel('density', color=color)
ax2.tick_params(axis='y', labelcolor=color)
ax2.grid(color=color, alpha=0.3)

# plot set up
fig.subplots_adjust(hspace=-1.)
fig.tight_layout(rect=[0, 0.01, 1, 0.95])
plt.show()
