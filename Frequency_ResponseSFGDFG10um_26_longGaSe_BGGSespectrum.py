# -*- coding: utf-8 -*-
"""
Created on Mon Jun 06 10:49:21 2016

@author: AUO OPCPA

Response function calculation for EOS
1. M. Knorr, P. Steinleitner, J. Raab, I. Gronwald, P. Merkl, C. Lange, and R. Huber, "Ultrabroadband etalon-free detection of infrared transients by van-der-Waals contacted sub-10-µm GaSe detectors," Opt. Express 26(15), 19059 (2018).
2. S. Keiber, S. Sederberg, A. Schwarz, M. Trubetskov, V. Pervak, F. Krausz, and N. Karpowicz, "Electro-optic sampling of near-infrared waveforms," Nat. Photonics 10(3), (2016).
3. T. Kampfrath, J. Nötzold, and M. Wolf, "Sampling of broadband terahertz pulses with thick electro-optic crystals," Appl. Phys. Lett. 90(23), 1–4 (2007).

v.OES7. Lenard
- 7um input + GaSe phase matching for the whole crystal length
- 2Dplot for phase matching


v.OES10
- phase is involved ni calculation
    
v.OES11 2019.07.10. Lenard
- product is corrected with multiplication w: EOS(W) ~ w*E_p(w)*E_p(w-W)*E_THz(W)
see M. Porer, J.-M. Ménard, and R. Huber, "Shot noise reduced terahertz detection via spectrally postfiltered electro-optic sampling," Opt. Lett. 39(8), 2435 (2014).    

v.OES13 2019.07.11. Lenard
- series for angle is done by this version

v.OES14 2019.07.12. Lenard
EOS_process output: field_avg.txt is used to get Ek, and generate PSD_E
EOS_process output_ avg.spect.txt is just for comparision

v.OES16. 2019.07.18. Lenard
- uses GaSe1: gain_sumf_eoe: refr. index caculation in um

v.OES17. 2019.07.19. Lenard
- uses f_argument_sumf_eoe: (2*np.pi/wl1/(nET(wl1*1e6,theta)))*(np.exp(1j*dk*l)-1.0)/(1j*dk)

v.OES19. 2019.07.19. Lenard
- plots 3 Resp function and corrected Spect

v.OES19_10um 2020.04.16. Lenard
- plots 3 Resp function and corrected Spect
- working without FTIR

v.OES20_SFGDFG10um 2020.04.16. Lenard
- SFG parameters are optimized
- lambda max is working

v.21. 2020.04,20. Lenard
- w**2/k M. Knorr, P. Steinleitner, J. Raab, I. Gronwald, P. Merkl, C. Lange, and R. Huber, "Ultrabroadband etalon-free detection of infrared transients by van-der-Waals contacted sub-10-µm GaSe detectors," Opt. Express 26(15), 19059 (2018).

v.24. 2020.04.21. LEnard
- DFG is added choosing LPF(long-pass filter)

v.25. 2020.04.23. Lenard
- BPF is added

v.25. 2020.06.04. Igor
- BPF from data added


ToDo:
    - add: Fresnel-Refections: important for GaSe at 1.5um see ref.3. above for correct formula
    - add: freq. dependence of nonlinear index: 
        Millers-rule
        0. BEST: G. Gallot and D. Grischkowsky, "Electro-optic detection of terahertz radiation," J. Opt. Soc. Am. B 16(8), 1204 (1999).
        1. A. Leitenstorfer, S. Hunsche, J. Shah, M. C. Nuss, and W. H. Knox, "Detectors and sources for ultrabroadband electro-optic sampling: Experiment and theory," Appl. Phys. Lett. 74(11), 1516–1518 (1999).
        2. A. Singh, A. Pashkin, S. Winnerl, M. Welsch, C. Beckh, P. Sulzer, A. Leitenstorfer, M. Helm, and H. Schneider, "Up to 70 THz bandwidth from an implanted Ge photoconductive antenna excited by a femtosecond Er:fibre laser," Light Sci. Appl. 9(1), 30 (2020).
    - ROI_Sp[0] is currenty always 0 in the code later in S_SFG and S_DFG only MaxIndex is considered!!!!!
    - add BPF correctly to main function and FLAGS
"""
#import time
#import os
import numpy as np
from numpy.fft import fft,ifft,fftshift,ifftshift,fftfreq
#from scipy.integrate import quad
import matplotlib.pylab as plt
from matplotlib import transforms
from matplotlib import cm
#from matplotlib import rc  #for greek letters
#import scipy.integrate as integrate
import scipy.constants as const 
from scipy.interpolate import interp1d
from scipy import interpolate
from GaSe2 import * 

#%%SET FLAGS
FLAG_CLOSE_PLOTS = 0
FLAG_PLOTG = 0                  #2D PLOT phase matching "Gain" function 
FLAG_XLIM_AUTO = 1              #spect xlim auto zoom
FLAG_DEBUG = 0                  #add av_spect.txt EOS Spectrum and ifft(Ek)
FLAG_THREE = 0                  #plots three spect for SPF values
"""ToDo: ROI_Sp[0] is currenty always 0 in the code later in S_SFG and S_DFG only MaxIndex is considered!!!!!"""
FLAG_ROISP = 'full'             #full: full sample beam wavelength range for ROI_Sp
#ROI_Sp = [1200,1700]                  #ROI_Sp
#ROI_Sp = [1090,2453]             #to use from the FROG file
#ROI_Sp = [1111,1898]             #to use from the FROG file
maxMIRwavel = 50e-6             #max wavelength where MIR is reasonable: required for minW
minW = 3e8/maxMIRwavel/1e12     #THz, minimum Omega to avoid division and sqrt errors in Sellmeier problems in S_SFG/S_DFG and GaSe.P...
print('max MIR wavel {}um = {}THz, (defines the minimum shift)'.format(maxMIRwavel*1e6,minW))
#%%SET PATHS

#Detector Response
fname_DetResp = 'InGaAsResp.txt'

#Sample Pulse
#Samp_filepath = "/Users/ityulnev/Documents/Response_function/1_6um_SamplePulse_Spectra/Ard30_P20_air1.txt" #Spectrum April 2020
Samp_filepath = "/Users/ityulnev/Documents/Response_function/3umBGGSe_3umcharacterization/1500_sampling/27bars.txt" #Spectrum June 2020
#Samp_filepath = "/Users/ityulnev/Documents/Response_function/1_6um_SamplePulse_Spectra/FROG_fs16mm_2p5W_frog3/Speck.txt" #Frog May 2020
#Samp_filepath = "/Users/ityulnev/Documents/Response_function/1_6um_SamplePulse_Spectra/FROGdata_sample/14h57m05sSpectrum_2p5W.txt" #Old Spectrum
Samp_filepath.replace("//",'/')


#EOS Signal
#ZGB
EOS_filepath_E = r"/Users/ityulnev/Documents/Response_function/BGGSe_intrapulse/EOS_ZGP/OUTdata_sam_EOS_ZGP/E_field_avg.txt"
EOS_filepath_Spec = r"/Users/ityulnev/Documents/Response_function/BGGSe_intrapulse/EOS_ZGP/OUTdata_sam_EOS_ZGP/spect_avg.txt"
#BGGSe
#EOS_filepath_E=r"/Users/ityulnev/Documents/Response_function/Measured_EOS_Efield/OUTdata_sam_20bar_50scans_new_v2/E_field_avg.txt"
#EOS_filepath_Spec=r"/Users/ityulnev/Documents/Response_function/Measured_EOS_Efield/OUTdata_sam_20bar_50scans_new_v2/spect_avg.txt"

#Filter
#Edmundoptics
#Filt_filepath1 = "/Users/ityulnev/Documents/Response_function/Filters/EdmundOptics/Fil1400nm/Default Dataset.csv"


#Filt_filepath1 = "/Users/ityulnev/Documents/Response_function/Filters/EdmundOptics/Fil1400nm/Default Dataset.csv"
#Filt_filepath2 = "/Users/ityulnev/Documents/Response_function/Filters/EdmundOptics/Fil1450nm/Default Dataset.csv"
#Filt_filepath3 = "/Users/ityulnev/Documents/Response_function/Filters/EdmundOptics/Fil1475nm/Default Dataset.csv"
Filt_filepath1 = "/Users/ityulnev/Documents/Response_function/Filters/ChrisDigitized/transmittance.csv"
Filt_filepath2 = "/Users/ityulnev/Documents/Response_function/Filters/OptiLab_Hung_byFerenczKarpat/Fig3_DefaultDataset_NNsort.csv"
Filt_filepath3 = "/Users/ityulnev/Documents/Response_function/Filters/OptiLab_Hung_byFerenczKarpat/Fig4_DefaultDataset_Xsort.csv"
#Filt_filepath1 = "/Users/ityulnev/Documents/Response_function/Filters/Pixeltec/SWIR-Bandpass-Filter-1337nm-FWHM-210nm-102386783.txt"
#Filt_filepath2 = "/Users/ityulnev/Documents/Response_function/Filters/Pixeltec/SWIR-Bandpass-Filter-1297nm-FWHM-220nm-102386784.txt"
#Filt_filepath3 = "/Users/ityulnev/Documents/Response_function/Filters/QuantumDesign/Quotes/CT1350-220bp design.txt"

#


Filt_filepaths = (Filt_filepath1,Filt_filepath2,Filt_filepath3)

#%% Filters
#define SPF (short-pass filter) for SFG calculations
#SPF = [1400e-9,1420e-9,1434e-9] #short wavelength pass filter, cutoff wavelength in m
#SPF = [1410e-9,1430e-9,1450e-9] #use for spectrometer
#SPF = [1407e-9,1450e-9,1450e-9] #use for spectrometer
SPF = []
LPF = []                        #short wavelength pass filter for SFG, cutoff wavelength in m, empty if unused

#Bandpassfilter

#Check Delimiters of file
def check(mypath,mychar):
    with open(mypath) as f:
        datafile = f.readlines()
    for line in datafile:
        if mychar in line:
            return True
    return False  # Because you finished the search without finding


i=0
BPF = []
BPF_func=[]
for m in Filt_filepaths:
    if check(m,';'):
        mydel = ';'
        filter_temp = np.loadtxt(m,skiprows=1,usecols=(0,1),delimiter=mydel)
    elif check(m,','):
        mydel = ','
        filter_temp = np.loadtxt(m,skiprows=1,usecols=(0,1),delimiter=mydel)
    else:
        filter_temp = np.loadtxt(m,skiprows=1,usecols=(0,1))
        
    BPF_func.append(const.c/(filter_temp[:,0]*1e-9))
    if max(filter_temp[:,1])>1:
        factor_percent = 100
    else:
        factor_percent = 1
    BPF_func.append(abs(filter_temp[:,1])/factor_percent)
    BPF.append(i+1)
    i+=1
    
#BPF = [1490e-9,1475e-9,1510e-9]
#BPFright = [1430e-9,1425e-9,1450e-9]


# =============================================================================
# #ToDo:
# #define BPF (long-pass filter) + FLAG_NLP
# FLAG_NLP = 'SFG'
# BPF = [[1200e-9,1400e-9],[1200e-9,1420e-9],[]1610e-9,1630e-9,1666e-9] #use for spectrometer
# SPF = []                        #short wavelength pass filter for SFG, cutoff wavelength in m, empty if unused
# LPF = []                        #long wavelength pass filter for DFG, cutoff wavelength in m, empty if unused
# 
# =============================================================================
wl3 = 11e-6                     #[m] 1st plot for test: wl3 should around the center freq of the MIR spectrum
#l_cryst = 30e-6                 #[m]
l_cryst = 30e-6                 #[m]
#theta_deg = 17
theta_deg = 12.9 #12.0
#theta_deg = 12.1 #for right sided FTIR
#theta_deg = 18 #for symmetric FTIR spectrum
gcmap = cm.jet   # global colormap
cmap = np.linspace(0, .94, 3) 
colors = [ gcmap(x) for x in cmap ]
threshold = .01                 # >0.01 , to cut the edges

#%% plot settings
""" matplotlib settings """
font = {'weight' : 'normal',
        'size'   : 12}

plt.rc('font', **font)

if FLAG_CLOSE_PLOTS:
    plt.close("all")
#%% function definitions

def FindMaxIndex(Vector, value):
    minimum = abs(Vector[0] - value)
    MaxIndex = 0
    for i, v in enumerate(Vector[1:]):
        mini = abs(value - v)
        if mini < minimum:
            minimum = mini
            MaxIndex = i+1
    return MaxIndex

#gives the index of the center mass
#for equidistant array
def FindCenterMassIndex(data):   #data: 1D array
    return int(np.average(np.arange(len(data)), weights=data))   #np.arange generates the indexes from 0

#for all
def FindCenterMassIndex1(f, data):   #data: 1D array
    fstep = np.append(np.diff(f),f[-1]-f[-2])
    return int(np.average(np.arange(len(data)), weights=data*fstep))   #np.arange generates the indexes from 0

def FindCenterMassIndex2(f, data):   #data: 1D array
    fstep = np.append(np.diff(f),f[-1]-f[-2])
    weights=data*fstep
    ind = np.arange(len(data))
    cmii = np.nan_to_num(np.sum(ind*weights)/np.sum(weights))
    return int(cmii)   #np.arange generates the indexes from 0

def FINDEDGES1(Y,ratio):
    d = Y - (max(Y) * ratio)
    indexes = np.where(d > 0)[-1]
    return (indexes[0],indexes[-1])

def FINDEDGES2(Y,ratio):  #find edges only around main peak
    ind_peak = np.argmax(Y)
    d = Y - (max(Y) * ratio)
    ind_min = ind_peak - np.where(np.flipud(d)[-ind_peak:] < 0.0)[0][0]
    ind_max = ind_peak + np.where(d[ind_peak:] < 0.0)[0][0]
    return (ind_min,ind_max)


def FFTSf(f,Sf,Npoints):
    stepf = f[1] - f[0]
    t = fftshift(fftfreq(Npoints, d=stepf))  #shifts the axis to make it symmetric around zero
    Et = fftshift(fft(Sf,Npoints))
#    It = It/It.max()  
    angt=-np.angle(Et)                   #normalize for convienence
    return [t,Et,angt] 

def IFFTSf1(f,Sf,Npoints):
    if f[-1]<f[0]:
        f0 = np.flipud(f)
        Sf0 = np.flipud(Sf)
    else:
        f0 = f
        Sf0 = Sf
    f1 = np.linspace(f0[0],f0[-1],Npoints)
    Sf1real = np.interp(f1, f0, np.real(Sf0))
    Sf1imag = np.interp(f1, f0, np.imag(Sf0))
    Sf1 = Sf1real + 1j*Sf1imag
#    print max(Sf)
    stepf = f1[1] - f1[0]
    t = fftshift(fftfreq(Npoints, d=stepf))  #shifts the axis to make it symmetric around zero
    Et = (ifft((Sf1),Npoints))
#    It = It/It.max()  
    angt=-np.angle(Et)                   #normalize for convienence
    return [t,Et,angt] 

def IFFTSf2(f,Sf,Npoints):
    if f[-1]<f[0]:
        f0 = np.flipud(f)
        Sf0 = np.flipud(Sf)
    else:
        f0 = f
        Sf0 = Sf
    f1 = np.linspace(f0[0],f0[-1],Npoints)
    Sf1real = np.interp(f1, f0, np.real(Sf0))
    Sf1imag = np.interp(f1, f0, np.imag(Sf0))
    Sf1 = Sf1real + 1j*Sf1imag
#    print max(Sf)
    stepf = f1[1] - f1[0]
    t = fftshift(fftfreq(Npoints, d=stepf))  #shifts the axis to make it symmetric around zero
    Sf2 = np.append(np.flipud(Sf1),Sf1)
    Et = (ifft((Sf2),2*Npoints))
#    It = It/It.max()  
    angt=-np.angle(Et)                   #normalize for convienence
    return [t,Et[Npoints:],angt[Npoints:]] 


def FFTSt1(t,Et,Npoints, _nzp):            #FFT from intensity
    """ zero padding """
    if (_nzp>0):
        _di = t[1]-t[0]
        _t = np.arange(t[0]-_nzp*_di, t[-1]+_nzp*_di, _di)
        _E = _t*0.0
        _E[_nzp:_nzp+len(Et)]= Et
    else:
        _t = t
        _E = Et
    t1 = np.linspace(_t[0],_t[-1],Npoints)
    Etp = np.interp(t1, _t, _E)
    stepf = t1[1] - t1[0]
    f = fftshift(fftfreq(Npoints, d=stepf))  #shifts the axis to make it symmetric around zero
    SE = fftshift(fft(Etp,Npoints))
#    S = S/S.max()  
    angSE=-np.angle(SE)                   #normalize for convienence
    return [f,SE,angSE] 

def FFTSt2(t,Et,Npoints):            #FFT from intensity
    t01 = np.linspace(t[0],t[-1],Npoints)
    Etp = np.interp(t01, t, Et)

    I = np.abs(Et)**2
    thr = np.max(I)*0.000000001
    Ipeak = np.argmax(I)
    try:
        imin = np.where(I[:Ipeak]<thr)[0][-1]
        imax = Ipeak+np.where(I[Ipeak:]<thr)[0][0]
    except:
#   logging.warning("imin/imax error: tROI_s: [%f,%f], self.t[%f,%f] (line291)"%(tROI_s[0],tROI_s[1],self.t[0]*1e15,self.t[-1]*1e15))
        imin = 0
        imax = len(I)-1
    print ('tROI_s_auto: [%ffs,%ffs]'%(t01[imin]*1e15,t01[imax]*1e15))
    E1 = Etp[imin:imax]
    t1 = t01[imin:imax]
    step = t1[1] - t1[0]

    f = fftshift(fftfreq(Npoints, d=step))  #shifts the axis to make it symmetric around zero
    SE = fftshift(fft(E1,Npoints))
#    S = S/S.max()  
    angSE=-np.angle(SE)                   #normalize for convienence
    return [f,SE,angSE] 


def FWHM2(X,Y):
    half_max = max(Y) / 2.0
    #find when function crosses line half_max (when sign of diff flips)
    #take the 'derivative' of signum(half_max - Y[])
    d = np.sign(half_max - np.array(Y[0:-1])) - np.sign(half_max - np.array(Y[1:]))
    #plot(X,d) #if you are interested
    #find the left and right most indexes
    left_idx = np.where(d > 0)[0]
    right_idx = np.where(d < 0)[-1]
    return abs(X[right_idx] - X[left_idx]) #return the difference (full width)


def S_SFG_BPF(GaSe,Spectrum_F,MinIndex,MaxIndex,minW,cryst_theta,cryst_l,a_det,ax,i,BPF_func=[]):
    
    #filter
#        SPFF_CutOff = 3e8/SPFL_CutOff
#        LPFF_CutOff = 3e8/LPFL_CutOff
#        R2 = step_SPF(Spectrum_F[:,0],SPFF_CutOff)*step_LPF(Spectrum_F[:,0],LPFF_CutOff)

    interp1 = interpolate.interp1d(BPF_func[0],BPF_func[1],fill_value="extrapolate")
    R2 = interp1(Spectrum_F[:,0])  
    indexLR = np.where(R2>max(R2)/2)
    indexL = (np.where(Spectrum_F[:,0]<min(BPF_func[0])))[0]
    R2[indL]=0
    
    SPFF_CutOff = Spectrum_F[min(indexLR[0]),0]
    LPFF_CutOff = Spectrum_F[max(indexLR[0]),0]    
    #this efficieny is only the effect of the filter on the sampling beam directly
    A_Unfiltered = np.sum(a_det*Spectrum_F[:,1])
    A_Filtered = np.sum(a_det*Spectrum_F[:,1]*R2)
    Efficiency = ( np.abs(A_Filtered) / np.abs(A_Unfiltered) ) * 100.0   
#    ax[0].plot(Spectrum_F[:MaxIndex,0]/1e12,Spectrum_F[:MaxIndex,1]/np.max(Spectrum_F[:MaxIndex,1]))
    ax[0].plot(Spectrum_F[:,0]/1e12,R2,color=colors[i],
      label=r'Filter $\lambda$={:1.3f}-{:1.3f}um E={:1.2f}%'.format(3e14/SPFF_CutOff,3e14/LPFF_CutOff,Efficiency),linewidth=2) 
    ax[0].legend(loc='right')    
    
    #create a 2D array (E(w-W): EwloOmega) with the shifted Spectrum for numerical integration
    """Spectrum_F is based on wavlengths but converted to freq., however it is decreasing in freqs.
    Therefore shifting to the left means sum-frequency"""
    EwloOmega = np.zeros((MaxIndex,MaxIndex)) +1j*np.zeros((MaxIndex,MaxIndex))  
    Omega=np.zeros(MaxIndex)
    for W in np.arange(MaxIndex):
        c1_ampl = np.append(np.sqrt(Spectrum_F[W:MaxIndex,1]),np.zeros(W))      #E=sqrt(PSD) if W+MaxIndex<len(Spectrum_F): MaxIndex is the last index 
        c1_ph   = np.append(Spectrum_F[W:MaxIndex,2],np.zeros(W))
        EwloOmega[:,W] = c1_ampl[:MaxIndex]*np.exp(-1j*c1_ph[:MaxIndex]) 
        Omega[W]=Spectrum_F[0,0]-Spectrum_F[W,0]        

    #Calculate the product M2=R*w/n(w)*E(W)*E(w-W):  Unfiltered R=1
    R = 1
    M = np.zeros((MaxIndex,MaxIndex)) + 1j*np.zeros((MaxIndex,MaxIndex))        #wo spectral filter
    M2 = np.zeros((MaxIndex,MaxIndex)) +1j*np.zeros((MaxIndex,MaxIndex))        #with spectral filter
    P2 = M2.copy()
    
    #define parameters for C, P, T, Kampfrath, J. Nötzold, and M. Wolf, "Sampling of broadband terahertz pulses with thick electro-optic crystals," Appl. Phys. Lett. 90(23), 1–4 (2007).
    Spectrum_Fcmplx = np.sqrt(Spectrum_F[:MaxIndex,1])*np.exp(1j*Spectrum_F[:MaxIndex,2])
    k = 2*np.pi*Spectrum_F[:MaxIndex,0]*GaSe.nO(3e8/Spectrum_F[:MaxIndex,0])/3e8
    KHI_2 = khi2_class(deff0=57.5e-12, wl20=1.65e-6, wl30=11.0e-6,theta0=np.radians(12.9),NLP0='SFG')   #deff0: [pV/m]
    khi2_SFG = 1 #KHI_2.khi2_sumf(wl2i=3e8/Spectrum_F[:MaxIndex,0], wl3i=11.0e-6,thetai=cryst_theta)
    C = (2*np.pi*Spectrum_F[:MaxIndex,0])**2/k*khi2_SFG
    
    
    #Calculate the product M2=R2*w/n(w)*E(W)*E(w-W): Filtered: R2 defined with R_Cutoff
    for W in np.arange(MaxIndex):
        P2[:,W] = GaSe.P_sumf_eoe(3e8/Spectrum_F[:MaxIndex,0],np.nan_to_num(3e8/Omega[W]),cryst_theta,cryst_l)
        #3e8/Omega[W] is close to inf at W==0
        if Omega[W]/1e12 > minW: 
            P2[:,W] = GaSe.P_sumf_eoe(3e8/Spectrum_F[:MaxIndex,0],3e8/Omega[W],cryst_theta,cryst_l)
        else:
            P2[:,W] = Spectrum_F[:MaxIndex,0]*0.0
            
            
        """ToDo: sqrt(PSD), or use the SpectrumF_cmplx, R2 is wrong: apply stepf line by line
        follow: Eq.2. from T. Kampfrath, J. Nötzold, and M. Wolf, "Sampling of broadband terahertz pulses with thick electro-optic crystals," Appl. Phys. Lett. 90(23), 1–4 (2007).
        """
        
#        T = GaSe.T[:,W] * a_det[:MaxIndex]* R
        T = a_det[:MaxIndex]* R
        M[:,W]  = np.conj(Spectrum_Fcmplx)*EwloOmega[:,W] * C * P2[:,W] * T
        M2[:,W] = M[:,W] * R2[:MaxIndex]

    #integrate for w
    I = np.zeros(MaxIndex) + 1j*np.zeros(MaxIndex)
    I2 = np.zeros(MaxIndex) +1j*np.zeros(MaxIndex)
#    I_store = np.zeros((MaxIndex-1-MinIndex,MaxIndex))
    for d in np.arange(MaxIndex):
#        for p in np.arange(MaxIndex-1):
#            I_store[p,d] = M[p,d] * np.abs(Spectrum_F[p,0] - Spectrum_F[p+1,0])
#        I[d] = np.sum(I_store[:,d])
        I[d] = np.sum(np.real(M[:,d])) + 1j*np.sum(np.imag(M[:,d]))  
        I2[d] = np.sum(np.real(M2[:,d])) + 1j*np.sum(np.imag(M2[:,d]))  

   
    #Calculate efficiency and plotting    
    
    Efficiency = ( np.sum(np.abs(I2)) / np.sum(np.abs(I)) ) * 100.0
    max_ind    = np.argmax(np.abs(I2))
    max_wavel  = 3e8/Omega[max_ind]*1e6
    cmass_ind  = FindCenterMassIndex1(Omega,I2)
#    cmass_ind  = FindCenterMassIndex2(Omega,np.abs(I2))
    cmass_wavel= 3e8/Omega[cmass_ind]*1e6
    print ('{0}: max wavelength: {1}um\t center of mass: {2}'.format(i,max_wavel, cmass_wavel))
#    if i==0: 
#        ax[1].plot(Omega/1e12,np.abs(I)/np.max(np.abs(I)),'--',color=colors[i],label='Unfiltered')      #normalized ot unfilterd response function
##        w_ = 10    #example for plotting
#        w_ = 10#np.where(Spectrum_F[:,0]<RF_CutOff)[0][-1]    #example for plotting
#        ax[1].plot(Omega/1e12,np.abs(P2[w_])/np.max(np.abs(P2[w_])),'-.',color=colors[i],alpha=0.5, label='Phase matching factor[W={:1.0f}THz]'.format(Spectrum_F[w_,0]/1e12))
#    ax[1].plot(Omega/1e12,np.abs(I2)/np.max(np.abs(I)),'.-',color=colors[i],label=r'$\lambda_c$ ={:1.1f}um $\lambda_m$ ={:1.1f}um E={:1.2f}%'.format(cmass_wavel,max_wavel,Efficiency)) 
##    ax[1].plot(Omega/1e12,np.abs(I),'--',color=colors[i],label='Unfiltered raw')
##    ax[1].plot(Omega/1e12,np.abs(I2),'.-',color=colors[i],label=r'raw $\lambda_c$ ={0:1.3f}um E={1:2.2f}%'.format(cmass_wavel,Efficiency)) 
#    ax[1].legend(loc='right')
#    
    return [Omega,I2/np.max(I2)]
    

def S_SFG(GaSe,R_CutOff,Spectrum_F,MinIndex,MaxIndex,minW,cryst_theta,cryst_l,a_det,ax,i):
    #filter
    RF_CutOff = 3e8/R_CutOff
    R2 = step_SPF(Spectrum_F[:,0],RF_CutOff)
    
    #this efficieny is only the effect of the filter on the sampling beam directly
    A_Unfiltered = np.sum(a_det*Spectrum_F[:,1])
    A_Filtered = np.sum(a_det*Spectrum_F[:,1]*R2)
    Efficiency = ( np.abs(A_Filtered) / np.abs(A_Unfiltered) ) * 100.0   
#    ax[0].plot(Spectrum_F[:MaxIndex,0]/1e12,Spectrum_F[:MaxIndex,1]/np.max(Spectrum_F[:MaxIndex,1]))
    ax[0].plot(Spectrum_F[:,0]/1e12,R2,color=colors[i],
      label=r'Filter $\lambda$={0:1.3f}um E={1:2.2f}%'.format(3e14/RF_CutOff,Efficiency)) 
    ax[0].legend(loc='right')    
    
    #create a 2D array (E(w-W): EwloOmega) with the shifted Spectrum for numerical integration
    """Spectrum_F is based on wavlengths but converted to freq., however it is decreasing in freqs.
    Therefore shifting to the left means sum-frequency"""
    EwloOmega = np.zeros((MaxIndex,MaxIndex)) +1j*np.zeros((MaxIndex,MaxIndex))  
    Omega=np.zeros(MaxIndex)
    for W in np.arange(MaxIndex):
        c1_ampl = np.append(np.sqrt(Spectrum_F[W:MaxIndex,1]),np.zeros(W))      #E=sqrt(PSD) if W+MaxIndex<len(Spectrum_F): MaxIndex is the last index 
        c1_ph   = np.append(Spectrum_F[W:MaxIndex,2],np.zeros(W))
        EwloOmega[:,W] = c1_ampl[:MaxIndex]*np.exp(-1j*c1_ph[:MaxIndex]) 
        Omega[W]=Spectrum_F[0,0]-Spectrum_F[W,0]        

    #Calculate the product M2=R*w/n(w)*E(W)*E(w-W):  Unfiltered R=1
    R = 1
    M = np.zeros((MaxIndex,MaxIndex)) + 1j*np.zeros((MaxIndex,MaxIndex))        #wo spectral filter
    M2 = np.zeros((MaxIndex,MaxIndex)) +1j*np.zeros((MaxIndex,MaxIndex))        #with spectral filter
    P2 = M2.copy()
    
    #define parameters for C, P, T, Kampfrath, J. Nötzold, and M. Wolf, "Sampling of broadband terahertz pulses with thick electro-optic crystals," Appl. Phys. Lett. 90(23), 1–4 (2007).
    Spectrum_Fcmplx = np.sqrt(Spectrum_F[:MaxIndex,1])*np.exp(1j*Spectrum_F[:MaxIndex,2])
    k = 2*np.pi*Spectrum_F[:MaxIndex,0]*GaSe.nO(3e8/Spectrum_F[:MaxIndex,0])/3e8
    KHI_2 = khi2_class(deff0=57.5e-12, wl20=1.65e-6, wl30=11.0e-6,theta0=np.radians(12.9),NLP0='SFG')   #deff0: [pV/m]
    khi2_SFG = 1 #KHI_2.khi2_sumf(wl2i=3e8/Spectrum_F[:MaxIndex,0], wl3i=11.0e-6,thetai=cryst_theta)
    C = (2*np.pi*Spectrum_F[:MaxIndex,0])**2/k*khi2_SFG
    
    
    #Calculate the product M2=R2*w/n(w)*E(W)*E(w-W): Filtered: R2 defined with R_Cutoff
    for W in np.arange(MaxIndex):
        P2[:,W] = GaSe.P_sumf_eoe(3e8/Spectrum_F[:MaxIndex,0],np.nan_to_num(3e8/Omega[W]),cryst_theta,cryst_l)
        #3e8/Omega[W] is close to inf at W==0
        if Omega[W]/1e12 > minW: 
            P2[:,W] = GaSe.P_sumf_eoe(3e8/Spectrum_F[:MaxIndex,0],3e8/Omega[W],cryst_theta,cryst_l)
        else:
            P2[:,W] = Spectrum_F[:MaxIndex,0]*0.0
            
            
        """ToDo: sqrt(PSD), or use the SpectrumF_cmplx, R2 is wrong: apply stepf line by line
        follow: Eq.2. from T. Kampfrath, J. Nötzold, and M. Wolf, "Sampling of broadband terahertz pulses with thick electro-optic crystals," Appl. Phys. Lett. 90(23), 1–4 (2007).
        """
        
#        T = GaSe.T[:,W] * a_det[:MaxIndex]* R
        T = a_det[:MaxIndex]* R
        M[:,W]  = np.conj(Spectrum_Fcmplx)*EwloOmega[:,W] * C * P2[:,W] * T
        M2[:,W] = M[:,W] * R2[:MaxIndex]

    #integrate for w
    I = np.zeros(MaxIndex) + 1j*np.zeros(MaxIndex)
    I2 = np.zeros(MaxIndex) +1j*np.zeros(MaxIndex)
#    I_store = np.zeros((MaxIndex-1-MinIndex,MaxIndex))
    for d in np.arange(MaxIndex):
#        for p in np.arange(MaxIndex-1):
#            I_store[p,d] = M[p,d] * np.abs(Spectrum_F[p,0] - Spectrum_F[p+1,0])
#        I[d] = np.sum(I_store[:,d])
        I[d] = np.sum(np.real(M[:,d])) + 1j*np.sum(np.imag(M[:,d]))  
        I2[d] = np.sum(np.real(M2[:,d])) + 1j*np.sum(np.imag(M2[:,d]))  

   
    #Calculate efficiency and plotting    
    
    Efficiency = ( np.sum(np.abs(I2)) / np.sum(np.abs(I)) ) * 100.0
    max_ind    = np.argmax(np.abs(I2))
    max_wavel  = 3e8/Omega[max_ind]*1e6
    cmass_ind  = FindCenterMassIndex1(Omega,I2)
#    cmass_ind  = FindCenterMassIndex2(Omega,np.abs(I2))
    cmass_wavel= 3e8/Omega[cmass_ind]*1e6
    print ('{0}: max wavelength: {1}um\t center of mass: {2}'.format(i,max_wavel, cmass_wavel))
    if i==0: 
        ax[1].plot(Omega/1e12,np.abs(I)/np.max(np.abs(I)),'--',color=colors[i],label='Unfiltered')      #normalized ot unfilterd response function
#        w_ = 10    #example for plotting
        w_ = 10#np.where(Spectrum_F[:,0]<RF_CutOff)[0][-1]    #example for plotting
        ax[1].plot(Omega/1e12,np.abs(P2[w_])/np.max(np.abs(P2[w_])),'-.',color=colors[i],alpha=0.5, label='Phase matching factor[W={:1.0f}THz]'.format(Spectrum_F[w_,0]/1e12))
    ax[1].plot(Omega/1e12,np.abs(I2)/np.max(np.abs(I)),'.-',color=colors[i],label=r'$\lambda_c$ ={:1.1f}um $\lambda_m$ ={:1.1f}um E={:1.2f}%'.format(cmass_wavel,max_wavel,Efficiency)) 
#    ax[1].plot(Omega/1e12,np.abs(I),'--',color=colors[i],label='Unfiltered raw')
#    ax[1].plot(Omega/1e12,np.abs(I2),'.-',color=colors[i],label=r'raw $\lambda_c$ ={0:1.3f}um E={1:2.2f}%'.format(cmass_wavel,Efficiency)) 
    ax[1].legend(loc='right')
    
    return [Omega,I2/np.max(I2)]



def S_DFG(GaSe,R_CutOff,Spectrum_F,MinIndex,MaxIndex,minW,cryst_theta,cryst_l,a_det,ax,i):
    #filter
    RF_CutOff = 3e8/R_CutOff
    R2 = step_LPF(Spectrum_F[:,0],RF_CutOff)
    
    #this efficieny is only the effect of the filter on the sampling beam directly
    A_Unfiltered = np.sum(a_det*Spectrum_F[:,1])
    A_Filtered = np.sum(a_det*Spectrum_F[:,1]*R2)
    Efficiency = ( np.abs(A_Filtered) / np.abs(A_Unfiltered) ) * 100.0   
#    ax[0].plot(Spectrum_F[:MaxIndex,0]/1e12,Spectrum_F[:MaxIndex,1]/np.max(Spectrum_F[:MaxIndex,1]))
    ax[0].plot(Spectrum_F[:,0]/1e12,R2,color=colors[i],
      label=r'Filter $\lambda$={0:1.3f}um E={1:2.2f}%'.format(3e14/RF_CutOff,Efficiency)) 
    ax[0].legend(loc='right')    

    
    #create a 2D array (E(w-W): EwloOmega) with the shifted Spectrum for numerical integration
    """Spectrum_F is based on wavlengths but converted to freq., however it is decreasing in freqs.
    Therefore shifting to the right means diff-frequency"""
    EwloOmega = np.zeros((MaxIndex,MaxIndex)) +1j*np.zeros((MaxIndex,MaxIndex))  
    Omega=np.zeros(MaxIndex)
    for W in np.arange(MaxIndex):
        c1_ampl = np.append(np.zeros(W),Spectrum_F[:MaxIndex-W,1])
        c1_ph   = np.append(np.zeros(W),Spectrum_F[:MaxIndex-W,2])
        EwloOmega[:,W] = c1_ampl[:MaxIndex]*np.exp(-1j*c1_ph[:MaxIndex]) 
        Omega[W]=Spectrum_F[0,0]-Spectrum_F[W,0]        

    #Calculate the product M2=R*w/n(w)*E(W)*E(w-W):  Unfiltered R=1
    R = 1
    M = np.zeros((MaxIndex,MaxIndex)) + 1j*np.zeros((MaxIndex,MaxIndex))        #wo spectral filter
    M2 = np.zeros((MaxIndex,MaxIndex)) +1j*np.zeros((MaxIndex,MaxIndex))        #with spectral filter
    P2 = M2.copy()
    
    #define parameters for C, P, T, Kampfrath, J. Nötzold, and M. Wolf, "Sampling of broadband terahertz pulses with thick electro-optic crystals," Appl. Phys. Lett. 90(23), 1–4 (2007).
    Spectrum_Fcmplx = np.sqrt(Spectrum_F[:MaxIndex,1])*np.exp(1j*Spectrum_F[:MaxIndex,2])
    k = 2*np.pi*Spectrum_F[:MaxIndex,0]*GaSe.nO(3e8/Spectrum_F[:MaxIndex,0])/3e8
    KHI_2 = khi2_class(deff0=57.5e-12, wl20=1.65e-6, wl30=11.0e-6,theta0=np.radians(12.9),NLP0='DFG')   #deff0: [pV/m]
    khi2_SFG = 1 #KHI_2.khi2_sumf(wl2i=3e8/Spectrum_F[:MaxIndex,0], wl3i=11.0e-6,thetai=cryst_theta)
    C = (2*np.pi*Spectrum_F[:MaxIndex,0])**2/k*khi2_SFG
    
    
    #Calculate the product M2=R2*w/n(w)*E(W)*E(w-W): Filtered: R2 defined with R_Cutoff
    for W in np.arange(MaxIndex):
        P2[:,W] = GaSe.P_diff_eoe(3e8/Spectrum_F[:MaxIndex,0],np.nan_to_num(3e8/Omega[W]),cryst_theta,cryst_l)
        #3e8/Omega[W] is close to inf at W==0
        if Omega[W]/1e12 > minW: 
            P2[:,W] = GaSe.P_diff_eoe(3e8/Spectrum_F[:MaxIndex,0],3e8/Omega[W],cryst_theta,cryst_l)
        else:
            P2[:,W] = Spectrum_F[:MaxIndex,0]*0.0
            
            
        """ToDo: sqrt(PSD), or use the SpectrumF_cmplx, R2 is wrong: apply stepf line by line
        follow: Eq.2. from T. Kampfrath, J. Nötzold, and M. Wolf, "Sampling of broadband terahertz pulses with thick electro-optic crystals," Appl. Phys. Lett. 90(23), 1–4 (2007).
        """
        
#        T = GaSe.T[:,W] * a_det[:MaxIndex]* R
        T = a_det[:MaxIndex]* R
        M[:,W]  = np.conj(Spectrum_Fcmplx)*EwloOmega[:,W] * C * P2[:,W] * T
        M2[:,W] = M[:,W] * R2[:MaxIndex]

    #integrate for w
    I = np.zeros(MaxIndex) + 1j*np.zeros(MaxIndex)
    I2 = np.zeros(MaxIndex) +1j*np.zeros(MaxIndex)
#    I_store = np.zeros((MaxIndex-1-MinIndex,MaxIndex))
    for d in np.arange(MaxIndex):
#        for p in np.arange(MaxIndex-1):
#            I_store[p,d] = M[p,d] * np.abs(Spectrum_F[p,0] - Spectrum_F[p+1,0])
#        I[d] = np.sum(I_store[:,d])
        I[d] = np.sum(np.real(M[:,d])) + 1j*np.sum(np.imag(M[:,d]))  
        I2[d] = np.sum(np.real(M2[:,d])) + 1j*np.sum(np.imag(M2[:,d]))  

   
    #Calculate efficiency and plotting    
    
    Efficiency = ( np.sum(np.abs(I2)) / np.sum(np.abs(I)) ) * 100.0
    max_ind    = np.argmax(np.abs(I2))
    max_wavel  = 3e8/Omega[max_ind]*1e6
#    cmass_ind  = FindCenterMassIndex1(Omega,I2)
    cmass_ind  = FindCenterMassIndex2(Omega,np.abs(I2))
    cmass_wavel= 3e8/Omega[cmass_ind]*1e6
    print ('{0}: max wavelength: {1}um\t center of mass: {2}'.format(i,max_wavel, cmass_wavel))
    if i==0: 
        ax[1].plot(Omega/1e12,np.abs(I)/np.max(np.abs(I)),'--',color=colors[i],label='Unfiltered')      #normalized ot unfilterd response function
#        w_ = 10    #example for plotting
        w_ = 10#np.where(Spectrum_F[:,0]<RF_CutOff)[0][-1]    #example for plotting
        ax[1].plot(Omega/1e12,np.abs(P2[w_])/np.max(np.abs(P2[w_])),'-.',color=colors[i],alpha=0.5,label='Phase matching factor[W={:1.0f}THz]'.format(Spectrum_F[w_,0]/1e12))
    ax[1].plot(Omega/1e12,np.abs(I2)/np.max(np.abs(I)),'.-',color=colors[i],label=r'$\lambda_c$ ={:1.1f}um $\lambda_m$ ={:1.1f}um E={:1.2f}%'.format(cmass_wavel,max_wavel,Efficiency)) 
    ax[1].legend(loc='right')
    
    return [Omega,I2/np.max(I2)]



def step_SPF(x,p): #short pass filterfor wavelength: i.e. high freq. are allowed
    return 1 * (x > p)

def step_LPF(x,p):
    return 1 * (x < p)

########################################### USER PART ###########################################
#%% MAIN
""" only execute the plotting part of the script if this is the main thread """
if __name__ == "__main__":
    GaSe = GaSe_class()  #from GaSe2.py
    if LPF: FLAG_NLP = 'DFG'
    if SPF: FLAG_NLP = 'SFG' #FLAG Nonlinear Process
    if BPF: FLAG_NLP = 'BPF'



    figa, axa = plt.subplots(2,figsize=(15,15))    
    figa.subplots_adjust(hspace=0.6)
    axa[0].set_title(FLAG_NLP +': Spectrum and filter\n theta=%.2lfdeg, length:%.2lfum'%(theta_deg,l_cryst*1e6),y=1.2,color='k')
#    axa[1].set_title('Normalized Detection Response',y=1.2,color ='k')
    axa[0].grid(b=True, which='both')
#    axa[1].grid(b=True, which='both')
    axa[0].set_xlabel('sampling frequency [THz]')
#    axa[1].set_xlabel('MIR frequency [THz]')
    
    
#    frogDir          = r"./FROGdata_sample/"
#    frogDir.replace("//",'/')
##    fname_FROGSp= frogDir+ "Speck.dat"
##    fname_FROGSp= frogDir+ "14h57m05sSpectrum_2p5W.txt"    #not equidistant: needs to interpolate
##    fname_FROGSp= "/Users/ityulnev/Documents/Response_function/FROG_fs16mm_2p5W_frog3/Speck.txt"  # Current FROG Spectrum, 3rd June 2020
##    fname_FROGSp= "/Users/ityulnev/Documents/Response_function/FROGdata_sample/14h57m05sSpectrum_2p5W.txt" # Old FROG  
##    fname_FROGSp= "/Users/ityulnev/Documents/Response_function/Sample_Spectrum/Ard30_P20_air1.txt"    # Measured Spectrum April 2020 
#    fname_FROGSp= "/Users/ityulnev/Documents/Response_function/3umBGGSe_3umcharacterization/1500_sampling/25bars.txt"    # Measured Spectrum April 2020 

    a_frog0 = np.loadtxt(Samp_filepath,skiprows=10,usecols=(0,1,2))
    if (a_frog0[0,0]>a_frog0[-1,0]):
        a_frog = np.flipud(a_frog0)
    else:
        a_frog = a_frog0
    print('FROG wavel range: [{0},{1}]'.format(a_frog[0,0],a_frog[-1,0]))
    if FLAG_ROISP == 'full':
        ROI_Sp = [0,0]
        ROI_Sp[0] = (a_frog[1,0])
        ROI_Sp[1] = (a_frog[-1,0])
        MaxInd = len(a_frog[:,0])
        MinInd = 0
    else:
        #define the ROI of the FROG spectrum by hand: it should be broader then the FTIR bandwidth
        MaxInd = np.where(a_frog[:,0]<ROI_Sp[1])[0][-1]
        MinInd = np.where(a_frog[:,0]<ROI_Sp[0])[0][-1]
    
    Spectrum_F0 = np.zeros((len(a_frog[MinInd:MaxInd,0]),3))
    Spectrum_F0[:,0] = 3e8/(a_frog[MinInd:MaxInd,0]*1e-9)     #wavel [nm-->Hz]
    Spectrum_F0[:,1] = abs(a_frog[MinInd:MaxInd,1])    #PSD   
    Spectrum_F0[:,2] = a_frog[MinInd:MaxInd,2]    #phase
    
    MinInd = 0
    MaxInd = len(Spectrum_F0)
    
    #Euler's formula to create complex
    Spectrum_Fcplx = np.sqrt(Spectrum_F0[:,1])*np.exp(1j*Spectrum_F0[:,2])     
#    Spectrum_FD[:,1] = np.sqrt(Spectrum_F0[:,1])
    
    #apply Detector responsibility
#    fname_DetResp = 'InGaAsResp.txt'
    a_det0 = np.loadtxt(fname_DetResp,skiprows=2)
    a_det0 = np.transpose(a_det0)
    a_det = a_det0
    a_det[0] = 3e8/a_det0[0]*1e9
    a_det01 = np.interp(np.flipud(Spectrum_F0[:,0]),np.flipud(a_det[0]),np.flipud(a_det[1]),left=0.00,right=0.00)
    a_det1 = np.flipud(a_det01)

    
    #plot one phase matching condition
    theta = np.radians(theta_deg)
#    theta = np.radians(18)
#    l_cryst = 30 #mm
#    wl2 = np.linspace(1.4,1.8,1000)
    wl2 = 3e8/Spectrum_F0[:,0]
#    wl3 = 10.0e-6 #um                      #wl3 is defined in SETFLAGS
    if len(SPF)>0: # 'SFG': 
        g_SFG  = GaSe.gain_sumf_eoe(wl2,wl3,theta,l_cryst)
        axa[0].plot(Spectrum_F0[:,0]/1e12,np.real(g_SFG)/np.max(np.real(g_SFG)),label='SFG Gain (phase matching) @%1.2lfum'%(wl3*1e6))
        Spectrum_F_SFG = Spectrum_F0.copy()
        Spectrum_F_SFG[:,1] = Spectrum_F0[:,1]*np.abs(g_SFG)
        axa[0].plot(Spectrum_F_SFG[:,0]/1e12,a_det1*(Spectrum_F_SFG[:,1])/np.max(Spectrum_F_SFG[:,1]), label='SFG FROG*DetResp*G')
    if len(LPF)>0: # 'DFG': 
        g_DFG = GaSe.gain_diff_eoe(wl2,wl3,theta,l_cryst)
        axa[0].plot(Spectrum_F0[:,0]/1e12,np.real(g_DFG)/np.max(np.real(g_DFG)),label='DFG Gain (phase matching) @%1.2lfum'%(wl3*1e6))
        Spectrum_F_DFG = Spectrum_F0.copy()
        Spectrum_F_DFG[:,1] = Spectrum_F0[:,1]*np.abs(g_DFG)
        axa[0].plot(Spectrum_F_DFG[:,0]/1e12,a_det1*(Spectrum_F_DFG[:,1])/np.max(Spectrum_F_DFG[:,1]), label='DFG FROG*DetResp*G')
    
    #early plotting: Det resposibility
    axa[0].plot(Spectrum_F0[:,0]/1e12,Spectrum_F0[:,1]/np.max(Spectrum_F0[:,1]),label='Input Spectrum')
    axa[0].plot(Spectrum_F0[:,0]/1e12,a_det1,'k--',label='Detector responsibility')
    axa[0].plot(Spectrum_F0[:,0]/1e12,a_det1*Spectrum_F0[:,1]/np.max(Spectrum_F0[:,1]), label='Input*DetResp')
    #axa[0].plot(Spectrum_FDG[:,0]/1e12,(Spectrum_FDG[:,1]/np.max(Spectrum_FDG[:,1])), label='FROG*DetResp*G')
#    axa[0].plot(Spectrum_F0[:MaxIndex,0]/1e12,R2,label=r'Filter $\lambda$=%1.2lfum'%(3*10**14/R_CutOff))
    axa[0].legend(loc='best')  
    
#%% phase matching wavelength: ROI_Sp values are required
    wl02 = np.linspace(ROI_Sp[0]*1.0e-9, ROI_Sp[1]*1e-9, 1000)  #k2<k3 and w1>w2>w3 for SFG and  w2>w1>w3 for DFG
#    plt.figure()
    dkk_SFG = GaSe.f_dk_sumf_eoe(wl02,wl3,np.radians(theta_deg))
    i_match_SFG = np.where(dkk_SFG[0:-2]*dkk_SFG[1:-1]<1)[0]            #search for zero position
    wl01_SFG = 1/(1/wl02[i_match_SFG][0] + 1/wl3)
    dkk_DFG = GaSe.f_dk_diff_eoe(wl02,wl3,np.radians(theta_deg))
    i_match_DFG = np.where(dkk_DFG[0:-2]*dkk_DFG[1:-1]<1)[0]            #search for zero position
    wl01_DFG = 1/(1/wl02[i_match_DFG][0] - 1/wl3)
    text_title = r'Phase mismatch (dk) at $\theta$={}$\degree$'.format(theta_deg)
    text_SFG ='SFG: {:1.3f}(o)+{:1.3f}(e)={:1.3f}(e)'.format(wl02[i_match_SFG][0]*1e6,wl3*1e6,wl01_SFG*1e6)
    text_DFG ='DFG: {:1.3f}(e)-{:1.3f}(e)={:1.3f}(o)'.format(wl02[i_match_DFG][0]*1e6,wl3*1e6,wl01_DFG*1e6)
    text_title = text_title + '\n' + text_SFG + '\n' + text_DFG
#    plt.title(text_title)
#    plt.plot(wl02*1e6,dkk_SFG, label = 'dk_SFG')
#    plt.plot(wl02*1e6,dkk_DFG, label = 'dk_DFG')
#    plt.xlabel('wl2 [um]')
#    plt.xlabel('phase mismatch')
#    plt.xlabel('wavelength [um]')
#    plt.grid()
#    plt.legend()
    
#%% 2D plot Gain function, phase matching
    
    if FLAG_PLOTG:
        gg = np.zeros((MaxInd-MinInd,MaxInd-MinInd))
        omegaa=np.zeros(MaxInd-MinInd)
        for ww in np.arange(MaxInd-MinInd):
            omegaa[ww]=Spectrum_F0[0,0]-Spectrum_F0[ww,0]    
            if omegaa[ww]/1e12 > minW: 
                if SPF: gg[:,ww] = np.abs(GaSe.gain_sumf_eoe(3e8/Spectrum_F0[:MaxInd,0],3e8/omegaa[ww],theta,l_cryst))*a_det1[:MaxInd]
                if LPF: gg[:,ww] = np.abs(GaSe.gain_diff_eoe(3e8/Spectrum_F0[:MaxInd,0],3e8/omegaa[ww],theta,l_cryst))*a_det1[:MaxInd]
#                gg[:,ww] = np.abs(GaSe.P_sumf_eoe(3e8/Spectrum_F0[:MaxInd,0],3e8/omegaa[ww],theta,l_cryst))*a_det1[:MaxInd]
            else:
                gg[:,ww] = Spectrum_F0[:MaxInd,0]*0.0
        figG,axG = plt.subplots(1,figsize=(15,15))    
        axG.pcolormesh(omegaa/1e12,Spectrum_F0[:MaxInd,0]/1e12,gg)
        axG.set_xlabel('MIR frequency [THz]')
        axG.set_ylabel('LO frequency [THz]')
        axG.set_title(FLAG_NLP+' Gain: phase matching, theta: %.2lfdeg, length: %.0lfum\n + Detector Response'%(np.degrees(theta),l_cryst*1e6),y=1.05)
        axG.grid(b=True, which='both')
        
        # first of all, the base transformation of the data points is needed
        axG1 = axG.twiny()
        base = plt.gca().transData
        rot = transforms.Affine2D().rotate_deg(90)
        # define transformed line
        line = axG1.plot(Spectrum_F0[:,0]/1e12,-(Spectrum_F0[:,1]/np.max(Spectrum_F0[:,1])), 'r--', transform= rot + base)
        axG1.set_xlim(0,10)
        plt.show()
    #plt.pcolormesh(gg)
#%% CALL FUNCTION S: spectral response calculation + plot SPF and responose functions
    i=0
    RESP = np.zeros([3,2,len(Spectrum_F0)]) + 1j*np.zeros([3,2,len(Spectrum_F0)])
    #short pass filter for sum freq. gen.
    if len(SPF)>0:
        for w_SPF in (SPF[0],SPF[1],SPF[2]): # Filter Wavelength in m 
            RESP[i] = S_SFG(GaSe,w_SPF,Spectrum_F0,MinInd,MaxInd,minW,theta,l_cryst,a_det1,axa,i) # S(Filter Wavelength,file name, min wavelength in nm, max wavelength in nm, minW: minimum freq in THz, axa: plot axis, i: numer of calls for increment colors)
            RESP[i] = S_SFG_BPF(GaSe,w_SPF,1250e-9,Spectrum_F0,MinInd,MaxInd,minW,theta,l_cryst,a_det1,axa,i) # S(Filter Wavelength,file name, min wavelength in nm, max wavelength in nm, minW: minimum freq in THz, axa: plot axis, i: numer of calls for increment colors)
        #    S( w,'15h56m25sSpectrum_350mW_31barpross_copy.txt',600,900) # S(Filter Wavelength,file name, min wavelength in nm, max wavelength in nm)
            i += 1
    #long pass filter for diff. freq. gen.
    if len(LPF)>0:
        for w in (LPF[0],LPF[1],LPF[2]): # Filter Wavelength in m 
            RESP[i] = S_DFG(GaSe,w,Spectrum_F0,MinInd,MaxInd,minW,theta,l_cryst,a_det1,axa,i) # S(Filter Wavelength,file name, min wavelength in nm, max wavelength in nm, minW: minimum freq in THz, axa: plot axis, i: numer of calls for increment colors)
            i += 1
    ## Band pass filter
    if len(BPF)>0:
        for w in Filt_filepaths: 
            BPF_filter = (BPF_func[2*i],BPF_func[2*i+1])
            RESP[i] = S_SFG_BPF(GaSe,Spectrum_F0,MinInd,MaxInd,minW,theta,l_cryst,a_det1,axa,i,BPF_filter) # S(Filter Wavelength,file name, min wavelength in nm, max wavelength in nm, minW: minimum freq in THz, axa: plot axis, i: numer of calls for increment colors)
            i += 1    
#%% set upper wavelength axis
    axa01 = axa[0].twiny()
    axa01.set_xlabel('wavelength [nm]')
    #axa01.format_coord = make_format(axa01, axa[0])
    axa01_xticks = axa[0].get_xticks()
    newlabel = 3e8/axa01_xticks/1e3 # labels of the xticklabels: the position in the new x-axis
    nposf = lambda x: 3e8/1e3/x # convert function: from Hz to nm
    newpos   = [nposf(x) for x in newlabel] 
    axa01.set_xticks(np.floor(newpos))
    axa01.set_xticklabels(np.floor(newlabel))
    axa0_lim = axa[0].get_xlim()
    axa01_lim = [3e8/i/1e3 for i in axa0_lim]
    axa01.set_xlim(axa0_lim)   
    
    
#    axa11 = axa[1].twiny()
#    axa11.set_xlabel('wavelength [nm]')
#    #axa01.format_coord = make_format(axa01, axa[0])
#    axa11_xticks = axa[1].get_xticks()
##    newlabel = 3e8/axa11_xticks/1e3 # labels of the xticklabels: the position in the new x-axis
#    newlabel = [np.inf if e == 0.0 else 3e8/e/1e3 for e in axa11_xticks]# labels of the xticklabels: the position in the new x-axis
#    nposf = lambda x: 3e8/1e3/x # convert function: from Hz to nm
#    newpos   = [nposf(x) for x in newlabel] 
#    axa11.set_xticks(np.floor(newpos))
#    axa11.set_xticklabels(np.floor(newlabel))
#    axa1_lim = axa[1].get_xlim()
#    axa11_lim = [3e8/i/1e3 for i in axa1_lim]
#    axa11.set_xlim(axa1_lim)       
    
    
    plt.show()
#%% FTIR spect
# =============================================================================
#     bg4 = np.loadtxt('./BGGSe DFG FTIR/1cmbg.txt')
#     s4 = np.loadtxt('./BGGSe DFG FTIR/1cm.txt')  #20 scans in 33s
# #    bg4 = np.loadtxt('C:/Users/Lenard/ownCloud/20190404_7umFTIR_EOS_30umGaSe/BGGSe DFG FTIR/2cmbg.txt')
# #    s4 = np.loadtxt('C:/Users/Lenard/ownCloud/20190404_7umFTIR_EOS_30umGaSe/BGGSe DFG FTIR/2cm.txt')  #20 scans in 33s
# #    bg4 = np.loadtxt('C:/Users/Lenard/ownCloud/20190404_7umFTIR_EOS_30umGaSe/BGGSe DFG FTIR/2cm348bg.txt')
# #    s4 = np.loadtxt('C:/Users/Lenard/ownCloud/20190404_7umFTIR_EOS_30umGaSe/BGGSe DFG FTIR/2cm348.txt')
# #    bg4 = np.loadtxt('C:/Users/Lenard/ownCloud/20190325_7umEOSbox/7um OPCPA/BGGS DFG/13.2pJ/13.2pJ_100ave_4cm-1_bg.txt')
# #    s4 = np.loadtxt('C:/Users/Lenard/ownCloud/20190325_7umEOSbox/7um OPCPA/BGGS DFG/13.2pJ/13.2pJ_100ave_4cm-1.txt')
#     # background correction
#     s4 = s4[:,1] - bg4[:,1]
#     
#     ftir_data = [bg4[:,0],s4] 
#     FTIR_raw = np.transpose(ftir_data)
#     
#     #OMEGA
#     WAVEL = FTIR_raw[:,0]
#     F0 = 3e8/(WAVEL*1e-9) #Lambda [nm -> m]
#     OMEGA = F0
#     #read FTIR Y values and smooth 
#     Sp_MIR0 = FTIR_raw[:,1]
#     _smoothSpec = 10
#     Sp_MIR = np.convolve(Sp_MIR0, np.ones(_smoothSpec), mode='same')/_smoothSpec
#     IMIRpeak = np.argmax(Sp_MIR)
#     thr = np.max(Sp_MIR)*threshold
#     imin_MIR = np.where(Sp_MIR[:IMIRpeak]<thr)[0][-1]
#     imax_MIR = IMIRpeak + np.where(Sp_MIR[IMIRpeak:]<thr)[0][0]
#     #    imin_OMEGA = np.where(OMEGA[IMIRpeak:]<2.0*np.pi*c/(sROI[0]*1e-6))[0][-1]
#     #    imax_OMEGA = IMIRpeak+np.where(OMEGA[:IMIRpeak]<2.0*np.pi*c/(sROI[1]*1e-6))[0][-1]
#     #        plt.figure()
#     #        plt.plot(OMEGA[imin_MIR:imax_MIR], Sp_MIR[imin_MIR:imax_MIR])
#     axa[2].plot(OMEGA[imin_MIR:imax_MIR]/1e12,Sp_MIR[imin_MIR:imax_MIR]/max(Sp_MIR[imin_MIR:imax_MIR]),label='FTIR MIR Spectrum')
#     
# =============================================================================
# # plotted for EOS
# #     if FLAG_PLOTG:
# #         axGG = axG.twinx() 
# #         axGG.plot(OMEGA[imin_MIR:imax_MIR]/1e12,Sp_MIR[imin_MIR:imax_MIR]/max(Sp_MIR[imin_MIR:imax_MIR]),'b--',label='FTIR MIR Spectrum')
# #         axGG.set_ylim(0.0,10.0)
# =============================================================================
#     
# =============================================================================
    
    
#%% EOS Spekt: optional for verification, because Ek is used to build the spectrum
    if FLAG_DEBUG:
    #    EOS_filepath_Spec=r"C:/Users/Lenard/ownCloud/20190320_7umEOS/OUTdata_sam_serie04_long/spect_avg.txt"
    #    EOS_filepath_Spec=r"C:/Users/Lenard/ownCloud/20190320_7umEOS/OUTdata_sam_serie04/spect_avg.txt"
    #    EOS_filepath_Spec=r"C:/Users/Lenard/ownCloud/20190531_grapheneribbonH03/pycodes/OUTdata_sam_A4_2p1_H06_1/spect_avg.txt"
        EOS_filepath_Spec=r"/Users/ityulnev/Documents/Response_function/OUTdata_sam_serie15_42_17300s/spect_avg.txt" #ZGB
#        EOS_filepath_Spec=r"/Users/ityulnev/Documents/Response_function/OUTdata_sam_20bar_50scans_new_v2/spect_avg.txt" #BGGSe
        
    #    EOS_filepath_Spec=r"C:/Users/Lenard/ownCloud/20190405_EOS30umGaSe/OUTspect_EOS1mmGaSe/spect_avg.txt"
        EOS_filepath_Spec = EOS_filepath_Spec.replace('//','/')
        EOS_Sp0_data = np.loadtxt(EOS_filepath_Spec,skiprows=1,usecols=(0,1,2))
        EOS_Sp = np.transpose(EOS_Sp0_data)
        EOS_Sp0 = 3e8/EOS_Sp[0]
        print 'EOS spect data points: ' +str(len(EOS_Sp0))
        RESP_intp = np.interp(EOS_Sp0,np.real(RESP[2][0]),np.abs(RESP[2][1]))
        #plt.plot(RESP[0]/1e12,RESP[1])
        axa[1].plot(EOS_Sp0/1e12,EOS_Sp[1]/max(EOS_Sp[1])+0.1,label='EOS Spectrum')
        axa[1].plot(EOS_Sp0/1e12,RESP_intp,color=colors[2],label='Response Function')
        [imin,imax]=FINDEDGES1(EOS_Sp[1],threshold)
    #    imin = 0
    #    imax = len(EOS_Sp[1])-1
        EOS_SpCorr = EOS_Sp[1]
        EOS_SpCorr[imin:imax] = EOS_Sp[1][imin:imax]/RESP_intp[imin:imax]
        EOS_SpCorr /= EOS_SpCorr.max()
        axa[1].plot(EOS_Sp0/1e12,EOS_SpCorr,label='EOS Spect Corrected')
    #    axa[1].semilogy(EOS_Sp0/1e12,EOS_SpCorr,label='EOS Corrected')
        
        
#%% read E-field from avg_field.txt
#    EOS_filepath_E=r"C:/Users/Lenard/ownCloud/20190320_7umEOS/OUTdata_sam_serie04_long/E_field_avg.txt"
#    EOS_filepath_E=r"C:/Users/Lenard/ownCloud/20190320_7umEOS/OUTdata_sam_serie04/E_field_avg.txt"
#    EOS_filepath_E=r"C:/Users/Lenard/ownCloud/20190531_grapheneribbonH03/pycodes/OUTdata_sam_A4_2p1_H06_1/E_field_avg.txt"
#    EOS_filepath_E=r"C:/Users/Lenard/ownCloud/20190531_grapheneribbonH03/pycodes/OUTdata_sam_A4_2p1_H06_1/E_field_avg.txt"
#    EOS_filepath_E=r".\OUTdata_sam_serie15_42_17300s/E_field_avg.txt"   #GaSe
#    EOS_filepath_E=r"/Users/ityulnev/Documents/Response_function/OUTdata_sam_serie15_42_17300s/E_field_avg.txt"   #ZGB
##    EOS_filepath_E=r"/Users/ityulnev/Documents/Response_function/OUTdata_sam_20bar_50scans_new_v2/E_field_avg.txt"   #BGGSe
#    EOS_filepath_E=r"C:/Users/Lenard/ownCloud/20190405_EOS30umGaSe/OUTspect_EOS1mmGaSe/E_field_avg.txt"
    
    EOS_filepath_E = EOS_filepath_E.replace('//','/')
    EOS_Ek0 = np.loadtxt(EOS_filepath_E,skiprows=1,usecols=(0,1,2))
    EOS_Ek = np.transpose(EOS_Ek0)
    print 'EOS time data points: ' +str(len(EOS_Ek[0]))
    
#%% Call FFTSf: EOS_Ek --> PSD_E
    Npoints = len(EOS_Ek[0]) if (len(EOS_Ek[0])>2**14) else 2**14
    [f_000,PSD_E000,PSD_000ph]=FFTSt1(EOS_Ek[0],(EOS_Ek[1]/max(EOS_Ek[1])),Npoints, Npoints)   #N_fft, N_zeropadding
#    [f_000,PSD_E000,PSD_000ph]=FFTSt2(EOS_Ek[0],(EOS_Ek[1]/max(EOS_Ek[1])),len(EOS_Ek[0]))
    f_00 = f_000[(len(f_000)/2):-1]
    PSD_E00 = PSD_E000[len(f_000)/2:-1]   #complex
    PSD_00ph = PSD_000ph[len(f_000)/2:-1] #only the phase
    wav_00 = 3e8/(f_00+1.0)                 # +1.0 to avoid "inf"
    PSD_00 = np.abs(PSD_E00)**2.0
    PSD_00 /= np.max(PSD_00)

    #plot spectrum
    [imin0,imax0] = FINDEDGES2(PSD_00,0.0001)
    PSD_01 = PSD_00 * 0.0
    PSD_01[imin0:imax0] = PSD_00[imin0:imax0]
    PSD_E01 = PSD_E00 * 0.0
    PSD_E01[imin0:imax0] = PSD_E00[imin0:imax0]
    print 'EOS fft data points: ' +str(len(PSD_01))
#    PSD_cm_ind = FindCenterMassIndex(PSD_01[imin0:imax0])
    PSD_cm_ind = FindCenterMassIndex1(f_00[imin0:imax0],PSD_01[imin0:imax0])
    PSD_cm_wavel = 3e8/f_00[imin0+PSD_cm_ind]*1e6
    print ('EOS PSD center mass: {0:1.3f}um'.format(PSD_cm_wavel))
    axa[1].plot(f_00/1e12,PSD_01,'.-',alpha=1,label=r'fft(EOS_Ek) $\lambda_c$={0:1.3f}um'.format(PSD_cm_wavel))
    
    
    #plot EOS to GAIN plot
    if FLAG_PLOTG:
        axGG = axG.twinx() 
        axGG.plot(f_00[imin0:imax0]/1e12,PSD_01[imin0:imax0]/max(PSD_01[imin0:imax0]),'b--',label='EOS Spectrum')
        axGG.set_ylim(0.0,10.0)

    
    
    #reconstruct the original EOS_Ek for test
    [t2,Et2,angt2]=IFFTSf1(f_00,PSD_E01,len(PSD_E01))  #with ifft
    imax=np.argmax(Et2)
    t2 -= t2[imax]

    #plot EOS_Ek
#%% EOS Trace    
    figb, axb = plt.subplots(1,figsize=(15,15))
    axb.plot(EOS_Ek[0]*1e15,EOS_Ek[1]/max(EOS_Ek[1]),'b-',alpha = 1, lw=1.5, label='EOS Measured')
    if FLAG_DEBUG:
        axb.plot(t2*1e15, np.real(Et2)/np.max(np.real(Et2)),'g-', alpha=1, label='ifft(fft(EOS_Ek)')
#    axb.plot(t2*1e15, np.imag(Et2)/np.max(np.imag(Et2)), label='Et2 imag')
    
    
    #ifft for the corrected EOS_Ek*RESP
    axaRESP = axa[1].twinx() 
    chi2 = []
    for j in range(max(len(SPF),len(LPF),len(BPF))):
#        if FLAG_THREE: 
        RESP_re = np.interp(f_00,np.real(RESP[j][0]),np.real(RESP[j][1]))
        RESP_im = np.interp(f_00,np.real(RESP[j][0]),np.imag(RESP[j][1]))
        RESP1 = RESP_re + 1j*RESP_im
        RESP1_abs = np.interp(f_00,np.real(RESP[j][0]),np.abs(RESP[j][1]))
        axaRESP.plot(3e8/wav_00/1e12,RESP1_abs,'--',color=colors[j],label='Response Function%d'%j)
#        plt.semilogy(3e8/wav_00/1e12,RESP1_abs,'--',color=colors[j],label='Response Function%d'%j)

        #FIND EDGES
        PSD_E00_corr = PSD_E00*0.0
        PSD_E00_corr[imin0:imax0] = np.divide(PSD_E00[imin0:imax0],RESP1[imin0:imax0], where= (RESP1[imin0:imax0] != 0.0))
        #division by ZERO is fixed by np.divide
        
        
        #calculate PSD corrected:
        PSD_E01_corr = (np.abs(PSD_E00_corr))**2
        PSD_E01_corr /= max(np.nan_to_num(PSD_E01_corr))
        chi2.append(sum (abs(abs(PSD_01)-abs(PSD_E01_corr)))) # Chi2 goodness of fit
        if SPF: axa[1].plot(3e8/wav_00/1e12,PSD_E01_corr,color=colors[j],alpha=0.8, label='fft(EOS_Ek_cor), SPF:%.2lfum'%(SPF[j]*1e6))
        if LPF: axa[1].plot(3e8/wav_00/1e12,PSD_E01_corr,color=colors[j],alpha=0.8, label='fft(EOS_Ek_cor), SPF:%.2lfum'%(LPF[j]*1e6))
        if BPF: axa[1].plot(3e8/wav_00/1e12,PSD_E01_corr,color=colors[j],alpha=0.8, label='fft(EOS_Ek_cor), '+'Deviation='+str(round(chi2[j],3)))
        

        #calculate Ek corrected:
        [t3,Et3,angt3]=IFFTSf1(f_00,PSD_E00_corr,len(PSD_E00))  #with ifft
        imax3=np.argmax(Et3)
        t3 -= t3[imax3]
    #    NIt3=abs(Et3*np.conj(Et3))
    #    NIt33 = NIt3/NIt3.max()  
        if SPF: axb.plot(t3[1:-1]*1e15,Et3[1:-1]/np.max(Et3),'-', color= colors[j], lw= 1.5, alpha =1, label='EOS cor, SPF:%.2lfum'%(SPF[j]*1e6))
        if LPF: axb.plot(t3[1:-1]*1e15,Et3[1:-1]/np.max(Et3),'-', color= colors[j], lw= 1.5, alpha =1, label='EOS cor, SPF:%.2lfum'%(LPF[j]*1e6))
        if BPF: axb.plot(t3[1:-1]*1e15,Et3[1:-1]/np.max(Et3),'-', color= colors[j], lw= 1.5, alpha =1, label='EOS cor, '+'BPF'+str(BPF[j]))

#        elif j==1: 
#            FLAG_THREE = 1
        plt.show()
    #PLOT SETTINGS
    if FLAG_XLIM_AUTO:
        axa[1].set_xlim(0.8*3e8/wav_00[imin0]/1e12,1.2*3e8/wav_00[imax0]/1e12)
        axb.set_xlim([-750,1250])
    
    axa[1].set_title('EOS and FTIR spect',y=1.2,color ='k')
    axa[1].grid(b=True, which='both')
    axa[1].set_xlabel('frequency [THz]')
    axa[1].legend(loc='best')    
    
    axa21 = axa[1].twiny()
    axa21.set_xlabel('wavelength [nm]')
    #axa01.format_coord = make_format(axa01, axa[0])
    axa21_xticks = axa[1].get_xticks()
    #print axa21_xticks
    newlabel = 3e8/axa21_xticks/1e3 # labels of the xticklabels: the position in the new x-axis
    #print newlabel
    nposf = lambda x: 3e8/1e3/x # convert function: from Hz to nm
    newpos   = [nposf(x) for x in newlabel] 
    #print newpos
    axa21.set_xticks(np.floor(newpos))
    #axa01.set_xticks(np.flipud(axa01_xticks))
    axa21.set_xticklabels(np.floor(newlabel))
    axa2_lim = axa[1].get_xlim()
    axa21_lim = [3e8/i/1e3 for i in axa2_lim]
    axa21.set_xlim(axa2_lim)       
    
    axb.set_title(r'$\theta$' + u': %.2lfdeg \n %s'%(theta_deg, EOS_filepath_E))
    axb.grid(b=True, which='both')
    axb.legend(loc="upper right")
#    axb.set_title(EOS_filepath_E)
    plt.show()
    

