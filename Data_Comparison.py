#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  5 10:05:41 2020

Read and plot N data files


@author: ityulnev
"""
import numpy as np
from numpy.fft import fft,ifft,fftshift,ifftshift,fftfreq
#from scipy.integrate import quad
import matplotlib.pylab as plt
from matplotlib import transforms
from matplotlib import cm
import scipy.constants as const 
from scipy.interpolate import interp1d


path1 = "/Users/ityulnev/Documents/Response_function/FROGdata_sample/"
path2 = "/Users/ityulnev/Documents/Response_function/FROG_fs16mm_2p5W_frog3/"
path3 = "/Users/ityulnev/Documents/Response_function/Sample_Spectrum/"
file1 = "14h57m05sSpectrum_2p5W.txt" # Old FROG data
file2 = "Speck.txt"                  # Current FROG Spectrum, 3rd June 2020
file3 = "Ard30_P20_air1.txt"         # Measured Spectrum April 2020 


path0 = "/Users/ityulnev/Documents/Response_function/3umBGGSe_3umcharacterization/1500_sampling/"
file1 = "20bars.txt"
file2 = "22bars.txt"
file3 = "25bars.txt"
file4 = "27bars.txt"
file5 = "30bars.txt"



files = [file1,file2,file3,file4,file5]
data_names = [path0+file1,path0+file2,path0+file3,path0+file4,path0+file5]

myspec=[]
myaxis=[]
myphase=[]
for m in data_names:
    m.replace('//','/')
    tempdata = np.loadtxt(m,skiprows=10,usecols=(0,1,2))
    if (tempdata[0,0]>tempdata[-1,0]):
        tempdata = np.flipud(tempdata)
    else:
        tempdata = tempdata
    
    myaxis.append(tempdata[:,0])
    myspec.append(abs(tempdata[:,1])/max(abs(tempdata[:,1])))
    myphase.append(tempdata[:,2])


#Plot
mylineW=4.0
fig, ax1 = plt.subplots(2,figsize=(15,15))
i=0
for m in data_names: 
    ax1[0].plot(myaxis[i],myspec[i],label=files[i],linewidth=mylineW)
    ax1[0].set_xlim(0,1)
    ax1[0].set_ylabel('Spectral Intensity [norm.]')
    ax1[0].set_xlim(1200,1850)
    ax1[0].set_xlabel('wavelength [nm]')
    ax1[0].grid(b=True, which='both')
    ax1[0].legend()
    ax1[0].set_title('1.6um Sample Pulse Spectra')


    ax1[1].plot(myaxis[i],myphase[i],label=files[i],linewidth=mylineW)
#    ax1[1].set_xlim()
    ax1[1].set_ylabel('Spectral Phase [rad.]')
    ax1[1].set_xlim(1200,1850)
    ax1[1].set_xlabel('wavelength [nm]')
    ax1[1].grid(b=True, which='both')
    ax1[1].legend()
    i += 1

plt.show()

#%% Filter
from scipy import interpolate
filt_path="/Users/ityulnev/Documents/Response_function/Filters/QuantumDesign/Quotes/CT1350-220bp design.txt"



BPF_func = np.loadtxt(filt_path,skiprows=1,usecols=(0,1))
#fig2, ax2 = plt.subplots(1,figsize=(15,15))
BPF_freq = const.c/(BPF_func[:,0]*1e-9)
#ax2.plot(BPF_freq,BPF_func[:,1])

ff=BPF_freq
ff2=Spectrum_F0[:,0]

#[min(ff*1e-12),max(ff*1e-12)]
#[min(ff2*1e-12),max(ff2*1e-12)]



#indL = (np.where(ff2<min(ff)))[0]
#indR = (np.where(ff<=max(ff2)))[0]
#intf = np.append(min(ff2), ff[indR])
#intFilt = np.append(0,BPF_func[indR,1])

#interp1 = interpolate.interp1d(intf, intFilt,fill_value="extrapolate")
#R2 = interp1(ff2)
#

interp1 = interpolate.interp1d(BPF_freq,(BPF_func[:,1]),fill_value="extrapolate")
R2s = interp1(ff2)
indL = (np.where(ff2<min(ff)))[0]
R2s[indL]=0


fig2, ax2 = plt.subplots(1,figsize=(15,15)) 
ax2.plot(1e-12*ff2,R2s/max(R2s),label='interpolated')
ax2.grid(b=True, which='both')
ax2.set_ylabel('Transmittance [norm.]')
ax2.set_xlabel('frequency [THz]')
ax2.plot(1e-12*ff,BPF_func[:,1]/max(BPF_func[:,1]),label='imported')
ax2.set_xlim(1e-12*min(ff2),1e-12*max(ff2))
ax2.legend()
ax2.set_title('Filter')

#%%
Filt_filepath1 = "/Users/ityulnev/Documents/Response_function/Filters/ChrisDigitized/transmittance.csv"
Filt_filepath2 = "/Users/ityulnev/Documents/Response_function/Filters/ChrisDigitized/transmittance.csv"
Filt_filepath3 = "/Users/ityulnev/Documents/Response_function/Filters/ChrisDigitized/transmittance.csv"

i=0
BPF_func=[]
fig2, ax2 = plt.subplots(1,figsize=(15,15)) 
for m in (Filt_filepath1,Filt_filepath2,Filt_filepath3):
    filter_temp = np.loadtxt(m,skiprows=1,usecols=(0,1),delimiter=',')
    BPF_func.append(const.c/(filter_temp[:,0]*1e-9))
    BPF_func.append(filter_temp[:,1]/100)
    i+=1
    ax2.plot(1e-12*ff2,R2s/max(R2s),label='interpolated')

ax2.grid(b=True, which='both')
ax2.set_ylabel('Transmittance [norm.]') 
ax2.set_xlabel('frequency [THz]')
