#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 16 10:24:00 2018

@author: lmaidment + Lenard
How to test:
    - SNLO: SFG and DFG
    QMix gives the angle and wavelengthes, SFG and DFG should give dk=0 for the two wavelengthes for eoe
"""

# calculations for GaSe SFG

import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import scipy.io as sio
import scipy.constants as const  

class GaSe_class():
    # ordinary index: GaSe
    def nO(self,wl):
        a1 = 10.6409
        b1 = 0.3788
        b2 = 0.1232
        c1 = 7090.7
        c2 = 2216.3
        n = (a1 + b1 / (wl**2 - b2) + c1 / (wl**2 - c2))**0.5
        return n

# =============================================================================
#     #G. D. Hovhannisyan, "Summary and difference frequency radiation generation in the field of few-cycle laser pulse propagating in GaSe," Opt. Mem. Neural Networks (Information Opt. 22(3), 135–147 (2013).
#     def nO(self,wl):
#         return np.sqrt(7.4437+0.3757/(wl**2-0.1260)-0.00154*wl**2)
#     
# =============================================================================
    # extraordinary: GaSe
    def nE(self,wl):
        a1 = 8.2477
        b1 = 0.2881
        b2 = 0.1669
        c1 = 4927.5
        c2 = 1990.1
        n = (a1 + b1 / (wl**2 - b2) + c1 / (wl**2 - c2))**0.5
        return n
    
    # angle dependant index
    def nET(self,wl, theta):
        n = np.sqrt(1 / ((np.cos(theta)**2 / self.nO(wl)**2) + 
                         (np.sin(theta)**2 / self.nE(wl)**2)))
        return n
    
    # snells law for ordinary
    def snellAng(self,n1,angIn,n2):
        angOut = np.arcsin(n1 * np.sin(angIn) / n2)
        return angOut
    
    #phase mismatch
    # gain for type 2 SFG in GaSe, w2 = probe wavelength (in um), w3 = mid-IR, theta = internal angle (rads), l = crystal length in um
    def f_dk_sumf_eoe(self,wl2,wl3,theta):
        #k1>k2>k3
        #phase mism: dk = -k1(sumf) + k2 + k3: Boyd pp.79.
        wl1 = 1/(1/wl2 + 1/wl3)
        #e(1250nm) = o(1500nm) + e(7500nm)
        dk = 2*np.pi*(-self.nET(wl1*1e6,theta)/wl1 + self.nO(wl2*1e6)/wl2 + self.nET(wl3*1e6,theta)/wl3)
        return dk
    
    def f_dk_diff_eoe(self,wl2,wl3,theta):
        #k2>k3>k1
        #phase mism: dk = k1(diff) - k2 + k3: Boyd pp.79.
        wl1 = 1/(1/wl2 - 1/wl3)
        #e(1250nm) = o(1500nm) + e(7500nm)
        dk = 2*np.pi*(self.nO(wl1*1e6)/wl1 - self.nET(wl2*1e6,theta)/wl2 + self.nET(wl3*1e6,theta)/wl3)
        return dk
    
    def gain_sumf_eoe(self,wl2,wl3,theta,l):
        dk = self.f_dk_sumf_eoe(wl2,wl3,theta)
        g = np.sinc(dk*l/2)**2
    #    g = g/max(g)
        return g
    
    def gain_diff_eoe(self,wl2,wl3,theta,l):
        dk = self.f_dk_diff_eoe(wl2,wl3,theta)
        g = np.sinc(dk*l/2)**2
    #    g = g/max(g)
        return g
    
    #   phase matching factor
    #   Eq.3. in 1. T. Kampfrath, J. Nötzold, and M. Wolf, "Sampling of broadband terahertz pulses with thick electro-optic crystals," Appl. Phys. Lett. 90(23), 1–4 (2007).
    def P_sumf_eoe(self,wl2,wl3,theta,l):
        dk = self.f_dk_sumf_eoe(wl2,wl3,theta)
        #supposing that khi2 doesn't change with wavelength, so it is neglected here
#        return np.where(dk != 0.0, (np.exp(1j*dk*l)-1.0)/dk, np.inf)
        return np.nan_to_num((np.exp(1j*dk*l)-1.0)/dk)
    
    def P_diff_eoe(self,wl2,wl3,theta,l):
        dk = self.f_dk_diff_eoe(wl2,wl3,theta)
        #supposing that khi2 doesn't change with wavelength, so it is neglected here
#        return np.where(dk != 0.0, (np.exp(1j*dk*l)-1.0)/dk, np.inf)
        return np.nan_to_num((np.exp(1j*dk*l)-1.0)/dk)

#for detailed description see: Miller's rule, Boyd chapter: 1.4.2, Eq.1.4.25., pp.26.
#use SNLO to get deff and calculate khi2 constant part for the selcted angle in Boyd: Eq.1.4.25.
class khi2_class(GaSe_class):
    def __init__(self, deff0, wl20, wl30, theta0, NLP0='SFG'):
        khi20 = 2*deff0  #Boyd: pp.50.
        if NLP0=='SFG': 
            wl10    = 1/(1/wl20 + 1/wl30)
            khi1_e_wl10 = np.sqrt((GaSe_class.nET(self,wl10,theta0))**2-1.0)  #Boyd 3.5.26
            khi1_o_wl20 = np.sqrt((GaSe_class.nO(self,wl20))**2-1.0)  #Boyd 3.5.26
            khi1_e_wl30 = np.sqrt((GaSe_class.nET(self,wl30,theta0))**2-1.0)  #Boyd 3.5.26
        elif NLP0=='DFG': 
            wl10    = 1/(1/wl20 - 1/wl30)
            khi1_e_wl10 = np.sqrt((GaSe_class.nO(self,wl10))**2-1.0)  #Boyd 3.5.26
            khi1_o_wl20 = np.sqrt((GaSe_class.nET(self,wl20,theta0))**2-1.0)  #Boyd 3.5.26
            khi1_e_wl30 = np.sqrt((GaSe_class.nET(self,wl30,theta0))**2-1.0)  #Boyd 3.5.26
        else:
            print('Error in GaSe.khi2.__init__(): choose NLP parameter: SFG or DFG')
        self.khi2const =khi20/khi1_e_wl10/khi1_o_wl20/khi1_e_wl30
#        print (self.khi2const)

    def khi2_sumf(self,wl2i, wl3i,thetai):
        wl1i    = 1/(1/wl2i + 1/wl3i)
        khi1_e_wl1 = np.sqrt((GaSe_class.nET(self,wl1i,thetai))**2-1.0)  #Boyd 3.5.26
        khi1_o_wl2 = np.sqrt((GaSe_class.nO(self,wl2i))**2-1.0)  #Boyd 3.5.26
        khi1_e_wl3 = np.sqrt((GaSe_class.nET(self,wl3i,thetai))**2-1.0)  #Boyd 3.5.26
        return self.khi2const*khi1_e_wl1*khi1_o_wl2*khi1_e_wl3

    def khi2_diff(self,wl2, wl3,theta):
        wl1    = 1/(1/wl2 - 1/wl3)
        khi1_e_wl1 = np.sqrt((GaSe_class.nO(self,wl1))**2-1.0)  #Boyd 3.5.26
        khi1_o_wl2 = np.sqrt((GaSe_class.nET(self,wl2,theta))**2-1.0)  #Boyd 3.5.26
        khi1_e_wl3 = np.sqrt((GaSe_class.nET(self,wl3,theta))**2-1.0)  #Boyd 3.5.26
        return self.khi2const*khi1_e_wl1*khi1_o_wl2*khi1_e_wl3



#%% example
#%% MAIN
""" only execute the plotting part of the script if this is the main thread """
if __name__ == "__main__":
    FLAG_NLP = 'SFG' #SFG or DFG   #Nonlinear Process is SFG or DFG
    l = 30e-6           #crystal thickness
#    wl2 = np.linspace(1.4e-6, 1.8e-6, 1000)
    
    #10um 
    theta = np.radians(12.9)            #internal angle in deg
    wl2 = np.linspace(1.0e-6, 2e-6, 1000)  #k2<k3 and w1>w2>w3 for SFG and  w2>w1>w3 for DFG
    wl3 = 11e-6
 
    GaSe = GaSe_class()
# =============================================================================
#     #7um 
#     theta = np.radians(18)
#     wl2 = np.linspace(1.0e-6, 2e-6, 1000)
#     wl3 = 7.5e-6
#     
# =============================================================================
    
# =============================================================================
#     wl2 = 1.5
#     wl3 = np.linspace(0.5, 6, 1000)
# =============================================================================

    #nO and nE in good agreement with refractiveindex.info plots
    plt.figure()
    plt.title('Refractive index')
    plt.plot(wl2*1e6,GaSe.nO(wl2*1e6), label= 'nO(wl2)')
    plt.plot(wl2*1e6,GaSe.nET(wl2*1e6,theta), label= 'nE(wl2,theta: %.2lf)'%np.degrees(theta))
    plt.plot(wl2*1e6,GaSe.nET(wl2*1e6,np.pi/2), label= 'nE(wl2,theta: %.2lf)'%np.degrees(np.pi/2))
    plt.xlabel('wl2 [um]')
    plt.ylabel('nO(wl2)')
    plt.grid()
    plt.legend()

    dkk_SFG = GaSe.f_dk_sumf_eoe(wl2,wl3,theta)
    dkk_DFG = GaSe.f_dk_diff_eoe(wl2,wl3,theta)
    plt.figure()
    plt.title('Phase mismatch')
    plt.plot(wl2*1e6,dkk_SFG, label = 'SFG: dk')
    plt.plot(wl2*1e6,dkk_DFG, label = 'DFG: dk')
    plt.xlabel('wl2 [um]')
    plt.grid()
    plt.legend()

    g0_SFG = GaSe.gain_sumf_eoe(wl2,wl3,theta,l)
    g0_DFG = GaSe.gain_diff_eoe(wl2,wl3,theta,l)
    plt.figure()
    plt.title(FLAG_NLP + ' Gain: sinc2')
    plt.plot(wl2*1e6,g0_SFG, label= 'SFG: |gain|2')
    plt.plot(wl2*1e6,g0_DFG, label= 'DFG: |gain|2')
    plt.xlabel('wl2 [um]')
    plt.ylabel('|gain|2')
    plt.grid()
    plt.legend()
#%% phase matching factor
#   Eq.3. in 1. T. Kampfrath, J. Nötzold, and M. Wolf, "Sampling of broadband terahertz pulses with thick electro-optic crystals," Appl. Phys. Lett. 90(23), 1–4 (2007).
    
#    ph_mism = (np.exp(1j*dkk*l)-1.0)/(1j*dkk)
    if FLAG_NLP == 'SFG': ph_mism = GaSe.P_sumf_eoe(wl2,wl3,theta,l)
    if FLAG_NLP == 'DFG': ph_mism = GaSe.P_diff_eoe(wl2,wl3,theta,l)
    plt.figure()
    plt.plot(wl2*1e6,np.real(ph_mism), label = FLAG_NLP +': Re{phase matching fact}')
    plt.plot(wl2*1e6,np.imag(ph_mism), label = FLAG_NLP +': Im{phase matching fact}')
    plt.xlabel('wl2 [um]')
    plt.title('Phase matching fact')
    plt.grid()
    plt.legend()
    
#%% khi2
    KHI_2 = khi2_class(deff0=57.5e-12, wl20=1.65e-6, wl30=11.0e-6,theta0=np.radians(12.9),NLP0='SFG')   #deff0: [pV/m]
    khi2_SFG = KHI_2.khi2_sumf(wl2i=wl2, wl3i=11.0e-6,thetai=theta)
    plt.figure()
    plt.plot(wl2*1e6,khi2_SFG, label = 'SFG: Khi2')
    plt.xlabel('wl2 [um]')
    plt.title('khi2 [pm/V]')
    plt.grid()
    plt.legend()
    